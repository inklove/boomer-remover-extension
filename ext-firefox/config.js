module.exports = {
    verbose: true,
    ignoreFiles: [
        'ext-firefox.iml',
        'icons/icon_off.svg',
        'config.js',
    ],
};