"use strict";

let storage = {

    addToBlacklist(domain) {
        browser.storage.local.get("blacklist").then(results => {
            let blacklist = results["blacklist"];
            let cleanDomain = cleanURL(domain);

            if (!blacklist.includes(cleanDomain)) {
                blacklist.push(cleanDomain);
                return browser.storage.local.set({
                    blacklist: blacklist
                });
            } else {
                console.log("domain already in blacklist");
            }

        })

    },

    addToWhitelist(domain) {
        browser.storage.local.get("whitelist").then(results => {
            let whitelist = results["whitelist"];
            let cleanDomain = cleanURL(domain);

            if (!whitelist.includes(cleanDomain)) {
                whitelist.push(cleanDomain);
                return browser.storage.local.set({
                    whitelist: whitelist
                });
            } else {
                console.log("domain already in whitelist");
            }
        })
    },

    removeFromBlacklist(domain) {
        browser.storage.local.get("blacklist").then(results => {
            let blacklist = results["blacklist"];
            let cleanDomain = cleanURL(domain);

            const index = blacklist.indexOf(cleanDomain);

            if (index > -1) {
                blacklist.splice(index, 1);
                return browser.storage.local.set({
                    blacklist: blacklist
                });
            }
        })
    },

    removeFromWhitelist(domain) {
        browser.storage.local.get("whitelist").then(results => {
            let whitelist = results["whitelist"];
            let cleanDomain = cleanURL(domain);

            const index = whitelist.indexOf(cleanDomain);

            if (index > -1) {
                whitelist.splice(index, 1);
                return browser.storage.local.set({
                    whitelist: whitelist
                });
            }
        })
    },

    checkDomainBlacklist(domain) {
        let cleanDomain = cleanURL(domain);
        let promise;
        return promise = new Promise(function (resolve) {
            browser.storage.local.get("blacklist").then(results => {
                let blacklist = results["blacklist"];

                const index = blacklist.indexOf(cleanDomain);
                resolve(index > -1);
            });
        })
    },

    checkDomainWhitelist(domain) {
        let cleanDomain = cleanURL(domain);
        let promise;
        return promise = new Promise(function (resolve) {
            browser.storage.local.get("whitelist").then(results => {
                let whitelist = results["whitelist"];

                const index = whitelist.indexOf(cleanDomain);
                resolve(index > -1);
            });
        })
    },

    getPrefs() {
        return browser.storage.local.get({
            mode: "blacklist",
            monitorChanges: true
        });
    },

    setMode(mode) {
        return browser.storage.local.set({mode: mode});
    },

    setReplaceChanges(monitorChanges) {
        return browser.storage.local.set({monitorChanges: monitorChanges});
    }

};

function cleanURL(url) {
    const startRegex = "^(https?://)?(www\d?\.)?";
    const finishRegex = "/.*";
    let newUrl = url.replace(new RegExp(startRegex, 'gi'), "");
    newUrl = newUrl.replace(new RegExp(finishRegex, 'gi'), "");
    return newUrl;
}