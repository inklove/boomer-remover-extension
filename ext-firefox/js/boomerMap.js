let dictionary = new Map();

dictionary.set('covid\-19', '🅱️OOMER-REMOVER-19');
dictionary.set('covid', '🅱️OOMER-REMOVER');
dictionary.set('coronavirus', 'Boomer Remover');
dictionary.set('orthocoronavirinae', 'orthoboomerremovae');
dictionary.set('coronaviridae', 'boomerremovae');
dictionary.set('Severe Acute Respiratory Syndrome Coronavirus', 'Severe Acute Boomer Remover Corona🅱️irus');
dictionary.set('SARS\-CoV', 'SABR-BoR');
dictionary.set('nCoV', 'nBoR');
//todo: add famous people/places? (e.g. Boris Johnson, Wuhan)


/*
 * After all the dictionary entries have been set, sort them by length.
 *
 * Because iteration over Maps happens by insertion order, this avoids
 * scenarios where words that are substrings of other words get substituted
 * first, leading to the longer word's substitution never triggering.
 *
 */
let tempArray = Array.from(dictionary);
tempArray.sort((pair1, pair2) => {
    // Each pair is an array with two entries: one or more covid related words, and its boomer replacement.
    // Ex: ['coronavirus', 'Boomer Remover']
    const firstWord = pair1[0];
    const secondWord = pair2[0];

    if (firstWord.length > secondWord.length) {
        // The first word should come before the second word.
        return -1;
    }
    if (secondWord.length > firstWord.length) {
        // The second word should come before the first word.
        return 1;
    }

    // The words have the same length, it doesn't matter which comes first.
    return 0;
});

// Now that the entries are sorted, put them back into a Map.
let sortedBoomerMap = new Map(tempArray);
