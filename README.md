# Boomer Remover

[![Firefox Version](https://img.shields.io/amo/v/boomer-remover)](https://addons.mozilla.org/it/firefox/addon/boomer-remover/)  

- [Boomer Remover](#boomer-remover)
    + [Download](#download)
    + [How To](#how-to)
    + [Screenshots](#screenshots)
    + [Contributing](#contributing)
    + [Credits](#credits)

Boomer Remover is a browser extension for Firefox and Chrome that replaces COVID-related words with boomer-remover related words. 
It was inspired by a random meme and developed to learn about browser extensions.

It has two operative modes:
* Blacklist Mode, where it's turned on by default except for pages in the blacklist
* Whitelist Mode, where it's turned off by default except for pages in the whitelist

### Download

* [Firefox Addon](https://addons.mozilla.org/it/firefox/addon/boomer-remover/)  
* At the moment, the extension isn't on Chrome yet. You can manually install it following [these instructions (from point 1)](https://developer.chrome.com/extensions/getstarted#manifest)

### How To

Install the extension, choose a mode (blacklist is the default one) and add/remove domains/subdomains to the whitelist/blacklist as you like. 

At the moment the lists work with sub-domains, this means that if you blacklist/whitelist `https://tg24.sky.it/intrattenimento/approfondimenti/foto/raffaello-sanzio-opere-storia.html` it will be stripped down to `tg24.sky.it` before being saved.

This will block/enable the whole subdomain  `tg24.sky.it` but it won't work on `sky.it` for example. For now, it seems like the best solution because you'll probably want to block on this level instead of on the page level.

The option "monitor changes" actively looks for new text inside the page to replace. This means that it may actively change what you are writing on a page (I saw it happens only once of regexr.com). I bear no responsibility for any situation that may arise from this. You can turn it off or switch to whitelist mode.

The workflow of the extension is:
```
is the mode blacklist? { 
(yes) => is the page on the blacklist? [ 
    (no) => replace words 
    (yes) => don't replace words 
    ], 
(no, whitelist) => is the page on the whitelist? [ 
    (yes) => replace words 
    (no) => don't replace words 
    ] 
}
```

* Replacements can be found inside [boomerMap.js](ext-firefox/js/boomerMap.js)
* Default blacklist and whitelist can be found inside [boomerMap.js](ext-firefox/js/setup.js)

If you need help, feel free to open an issue here (you'll need a free account).

### Screenshots

Screenshots are under the [images folder](./images).

![](images/04.png)

### Contributing

Contributions are welcome. I don't know javascript, so the code is probably ugly (maybe, I really don't know ¯\\_(ツ)_/¯ ).

This extension was developed with the Intellij IDEA Ultimate version, but It can easily be modified even with a notepad.

* [Chrome's official docs](https://developer.chrome.com/extensions/getstarted)
* [Mozilla's official docs](https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/WebExtensions)

Differences between Chrome and Mozilla:

Chrome uses chrome as namespace
```js
chrome.runtime.onInstalled.addListener(onInstalledSetup);
```
Mozilla uses browser
```js
browser.runtime.onInstalled.addListener(onInstalledSetup);
```
Chrome uses callbacks for async operations
```js
    chrome.storage.local.get("blacklist", function (results) {
        let blacklist = results.blacklist;
        if (!Array.isArray(blacklist))
            blacklist = [];
        for (let blocked of defaultBlacklist) {
            if (!blacklist.includes(blocked)){
                blacklist.push(blocked);
                updatedDefaultBlacklist = true
            }
        }
        if (updatedDefaultBlacklist)
            chrome.storage.local.set({blacklist: blacklist});
    });
```
Mozilla uses promises

```js
    browser.storage.local.get("blacklist").then(results => {
        let blacklist = results["blacklist"];
        if (!Array.isArray(blacklist))
            blacklist = [];
        for (let blocked of defaultBlacklist) {
            if (!blacklist.includes(blocked)){
                blacklist.push(blocked);
                updatedDefaultBlacklist = true
            }
        }
        if (updatedDefaultBlacklist)
            browser.storage.local.set({blacklist: blacklist});
    });
```

### Credits

Icon made with the help of [Dustpan Vectors by Vecteezy](https://www.vecteezy.com/free-vector/dustpan)
