/* initialise variables */
const whitelistAddBtn = document.getElementById('whitelistAdd');
const whitelistRemoveBtn = document.getElementById('whitelistRemove');
const blacklistAddBtn = document.getElementById('blacklistAdd');
const blacklistRemoveBtn = document.getElementById('blacklistRemove');
const cbMonitorChanges = document.getElementById('cbMonitorChanges');
const blacklistButtons = document.getElementById('buttonsBlacklist');
const whitelistButtons = document.getElementById('buttonsWhitelist');
const sbMode = document.getElementById('sMode');

/*  add event listeners to buttons and checkboxes   */

whitelistAddBtn.addEventListener('click', addWhitelist);
whitelistRemoveBtn.addEventListener('click', removeWhitelist);
blacklistAddBtn.addEventListener('click', addBlacklist);
blacklistRemoveBtn.addEventListener('click', removeBlacklist);
cbMonitorChanges.addEventListener('change', toggleMonitorChanges);
sbMode.addEventListener('change', changeMode);

/* load saved preferences on startup */

initialize();

function initialize() {

    storage.getPrefs(function (preferences) {
        //get the mode (whitelist or blacklist)
        let mode = preferences["mode"];
        // load the buttons
        toggleModeButtons(mode);

        // load mode select box
        for (let index=0; index < sbMode.length; index++){
            if (sbMode.options[index].value === mode) {
                sbMode.selectedIndex = index;
                break;
            }
        }

        // load monitor changes checkbox
        cbMonitorChanges.checked = preferences.monitorChanges;
    });

    loadTranslation()
}

function loadTranslation() {

    whitelistAddBtn.textContent = chrome.i18n.getMessage("add_whitelist");
    whitelistRemoveBtn.textContent = chrome.i18n.getMessage("remove_whitelist");

    blacklistAddBtn.textContent = chrome.i18n.getMessage("add_blacklist");
    blacklistRemoveBtn.textContent = chrome.i18n.getMessage("remove_blacklist");

    const optionsTitle = document.getElementById('tableTitleOptions');
    optionsTitle.textContent = chrome.i18n.getMessage("global_options");

    const labelMode = document.getElementById('labelMode');
    const optionWhitelist = document.getElementById('sWhitelist');
    const optionBlacklist = document.getElementById('sBlacklist');

    labelMode.textContent = chrome.i18n.getMessage("mode");
    optionWhitelist.textContent = chrome.i18n.getMessage("whitelist_mode");
    optionBlacklist.textContent = chrome.i18n.getMessage("blacklist_mode");


    const labelMonitor = document.getElementById('labelMonitor');
    labelMonitor.textContent = chrome.i18n.getMessage("monitor_cb");

    const labelReload = document.getElementById('reloadPage');
    labelReload.textContent = chrome.i18n.getMessage("reload_to_apply");

}

/* Callbacks */

function addWhitelist() {
    getCurrentWindowTab(function (url) {
        storage.addToWhitelist(url);
        toggleWhitelistButton(true)
    });
}

function removeWhitelist() {
    getCurrentWindowTab(function (url) {
        storage.removeFromWhitelist(url);
        toggleWhitelistButton(false)
    });
}

function addBlacklist() {
    getCurrentWindowTab(function (url) {
        storage.addToBlacklist(url);
        toggleBlacklistButton(true)
    });
}

function removeBlacklist() {
    getCurrentWindowTab(function (url) {
        storage.removeFromBlacklist(url);
        toggleBlacklistButton(false)
    });
}

function changeMode() {
    storage.setMode(this.value);
    toggleModeButtons(this.value);
}

function toggleMonitorChanges() {
    storage.setReplaceChanges(this.checked)
}

/*  Utilities   */
function getCurrentWindowTab(callback) {
    return chrome.tabs.query({active: true, currentWindow: true}, function (tabs) {
        callback(tabs[0].url);
    });
}

function toggleModeButtons(mode){
    if (mode === "blacklist") {
        whitelistButtons.classList.add("hidden");
        blacklistButtons.classList.remove("hidden");
        toggleBlacklistButton()
    } else {
        blacklistButtons.classList.add("hidden");
        whitelistButtons.classList.remove("hidden");
        toggleWhitelistButton()
    }
}

function toggleBlacklistButton(inBlacklist){
    if (inBlacklist === undefined ){

        getCurrentWindowTab(function (url) {
            storage.checkDomainBlacklist(url,function(result){
                toggleBlacklistButton(result)
            });
        });

    } else {
        if (inBlacklist) {
            blacklistAddBtn.classList.add("hidden");
            blacklistRemoveBtn.classList.remove("hidden");
        } else {
            blacklistRemoveBtn.classList.add("hidden");
            blacklistAddBtn.classList.remove("hidden");
        }
    }
}

function toggleWhitelistButton(inWhitelist){
    if (inWhitelist === undefined ){

        getCurrentWindowTab(function (url) {
            storage.checkDomainWhitelist(url,function(result){
                toggleWhitelistButton(result)
            });
        });

    } else {
        if (inWhitelist) {
            whitelistAddBtn.classList.add("hidden");
            whitelistRemoveBtn.classList.remove("hidden");
        } else {
            whitelistRemoveBtn.classList.add("hidden");
            whitelistAddBtn.classList.remove("hidden");
        }
    }
}