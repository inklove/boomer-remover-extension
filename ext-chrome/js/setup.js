const defaultBlacklist = [
    "web.whatsapp.com",
    "web.skype.com",
    "web.telegram.org",
    "mail.google.com",
    "mail.yahoo.com",
    "outlook.live.com",
    "inbox.com",
    "docs.google.com",
    "outlook.live.com",
    "office.com"
];

const defaultWhitelist = [
    "reddit.com",
    "old.reddit.com"
];

function updateDefaultBlacklist(defaultBlacklist) {
    let updatedDefaultBlacklist = false;
    chrome.storage.local.get("blacklist", function (results) {
        let blacklist = results.blacklist;
        if (!Array.isArray(blacklist))
            blacklist = [];
        for (let blocked of defaultBlacklist) {
            if (!blacklist.includes(blocked)){
                blacklist.push(blocked);
                updatedDefaultBlacklist = true
            }
        }
        if (updatedDefaultBlacklist)
            chrome.storage.local.set({blacklist: blacklist});
    });
}

function updateDefaultWhitelist(defaultWhitelist) {
    let updatedDefaultWhitelist = false;
    chrome.storage.local.get("whitelist", function (results) {
        let whitelist = results.whitelist;
        if (!Array.isArray(whitelist))
            whitelist = [];
        for (let blocked of defaultWhitelist) {
            if (!whitelist.includes(blocked)){
                whitelist.push(blocked);
                updatedDefaultWhitelist = true
            }
        }
        if (updatedDefaultWhitelist)
            chrome.storage.local.set({whitelist: whitelist});
    });
}

function onInstalledSetup(details) {
    updateDefaultBlacklist(defaultBlacklist);
    updateDefaultWhitelist(defaultWhitelist);
}

chrome.runtime.onInstalled.addListener(onInstalledSetup);
