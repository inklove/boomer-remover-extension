"use strict";

let storage = {

    addToBlacklist(domain) {
        chrome.storage.local.get("blacklist", function (results) {
            let blacklist = results.blacklist;
            let cleanDomain = cleanURL(domain);

            if (Array.isArray(blacklist)) {
                if (!blacklist.includes(domain))
                    blacklist.push(cleanDomain);
                else
                    console.log("domain already in blacklist");
            } else {
                //todo:initialize blacklist
                blacklist = [cleanDomain];
            }
            chrome.storage.local.set({blacklist: blacklist});
        })

    },

    addToWhitelist(domain) {
        chrome.storage.local.get("whitelist", function (results) {
            let whitelist = results.whitelist;
            let cleanDomain = cleanURL(domain);

            if (Array.isArray(whitelist)) {
                if (!whitelist.includes(domain))
                    whitelist.push(cleanDomain);
                else
                    console.log("domain already in whitelist");
            } else {
                whitelist = [cleanDomain];
            }
            chrome.storage.local.set({whitelist: whitelist});
        })

    },

    removeFromBlacklist(domain) {
        chrome.storage.local.get("blacklist", function (results) {
            let blacklist = results.blacklist;
            let cleanDomain = cleanURL(domain);

            if (Array.isArray(blacklist)) {

                const index = blacklist.indexOf(cleanDomain);
                if (index > -1) {
                    blacklist.splice(index, 1);
                    chrome.storage.local.set({
                        blacklist: blacklist
                    });
                }
            }
            else
                console.log("Error: couldn't find blacklist array");
        })

    },

    removeFromWhitelist(domain) {
        chrome.storage.local.get("whitelist", function (results) {
            let whitelist = results.whitelist;
            let cleanDomain = cleanURL(domain);

            if (Array.isArray(whitelist)) {

                const index = whitelist.indexOf(cleanDomain);
                if (index > -1) {
                    whitelist.splice(index, 1);
                    chrome.storage.local.set({
                        whitelist: whitelist
                    });
                }
            }
            else
                console.log("Error: couldn't find whitelist array");
        })

    },

    checkDomainBlacklist(domain, callback) {
        chrome.storage.local.get("blacklist", function (results) {
            let blacklist = results.blacklist;
            let cleanDomain = cleanURL(domain);

            if (Array.isArray(blacklist)) {
                const index = blacklist.indexOf(cleanDomain);
                callback(index > -1);
            } else {
                console.log("Error: couldn't find blacklist array");
            }
        });
    },

    checkDomainWhitelist(domain, callback) {
        chrome.storage.local.get("whitelist", function (results) {
            let whitelist = results.whitelist;
            let cleanDomain = cleanURL(domain);

            if (Array.isArray(whitelist)) {
                const index = whitelist.indexOf(cleanDomain);
                callback(index > -1);
            } else {
                console.log("Error: couldn't find whitelist array");
            }
        });
    },

    getPrefs(callback) {
        chrome.storage.local.get({
                mode: "blacklist",
                monitorChanges: true
            },
            function (results) {
                callback({
                    mode: results.mode,
                    monitorChanges: results.monitorChanges
                })
            });
    },

    setMode(mode) {
        chrome.storage.local.set({mode: mode});
    },

    setReplaceChanges(monitorChanges) {
        chrome.storage.local.set({monitorChanges: monitorChanges});
    }

};

function cleanURL(url) {
    const startRegex = "^(https?://)?(www\d?\.)?";
    const finishRegex = "/.*";
    let newUrl = url.replace(new RegExp(startRegex, 'gi'), "");
    newUrl = newUrl.replace(new RegExp(finishRegex, 'gi'), "");
    return newUrl;
}